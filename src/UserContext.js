import React from 'react'


// Create a context object
	// Context object is a different approach in passing information between components ang allows easier access by avoiding props-drilling

// type of object that can be used to store information that can be shared to other components within the aapp
const UserContext = React.createContext()

// The "Provider" component allows other components to consume/use the context object and supply the necessary information need to the context object
export const UserProvider = UserContext.Provider;

export default UserContext